<?php

/**
 * @file
 * User page callbacks for the multilingual_content_type module.
 */

/**
 * Replacement for node_add_page.
 */
function multilingual_content_type_add_page() {
  $item = menu_get_item();
  $content = system_admin_menu_block($item);
  // Bypass the node/add listing if only one content type is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto($item['href']);
  }
  foreach ($content as &$item) {
    // Type machine name will be the first page argument
    $page_arguments = unserialize($item['page_arguments']);
    // Check whether this has a node type, other items may be here too, see #1264662
    $type = isset($page_arguments[0]) ? $page_arguments[0] : NULL;
    if ($type) {
      // We just need to translate the description, the title is translated by the menu system
      // The string will be filtered (xss_admin) on the theme layer
      $item['description'] = multilingual_content_type_translate_type($type, 'description', $item['description'], array('sanitize' => FALSE));
    }
  }
  return theme('node_add_list', array('content' => $content));
}
